
/**
* Custom get response hook for getresponse visited page.
* Public API are
* - registerForm(formname : string) : void
* needs to be called inside the page where we need to trigger the event,
* for example a user bought stuff or compiled a form or whatever.
* - notify(void) : void
* Needs to be inside the `thank you` page and will read the previously set
* cookie (if exists), get the data, publish to GetResponse and delete the
* cookie.
*
* @param cookieName Name of the cookie to store
* @param cookieTime Lifetime of the cookie
*/
(function(name, time) {
  
  function AltavistaGetResponse(cookieName, cookieTime) {
    if (!window.gaSetUserId || typeof window.gaSetUserId !== 'function')
      throw new Error("[AltavistaGetResponse] requires `piwik` script from GetResponse in order to work.");
    
    this.sCookieName = 'AvGR_' + cookieName;
    this.nCookieTime = cookieTime;
  };
  
  AltavistaGetResponse.prototype.setCookieLife = function(days) {
    this.nCookieTime = days;
  };
  
  /**
  * Create a new cookie (or replace existing one) with our custom value (user email)
  * so that we can later on take it back and dispatch it to GetResponse.
  *
  * @param cValue value to set into the cookie
  * @return null
  */
  AltavistaGetResponse.prototype.setCookie = function(cValue) {
    let d = new Date();
    d.setTime(d.getTime() + (this.nCookieTime * 24 * 60 * 60 * 1000));
    const expires = "expires="+ d.toUTCString();
    document.cookie = this.sCookieName + "=" + cValue + ";" + expires + ";path=/";
  };
  
  /**
  * Retrieve the previously set cookie (if any) and return its value
  *
  * @return string value of the cookie or empty string
  */
  AltavistaGetResponse.prototype.getCookie = function() {
    // Get name followed by anything except a semicolon
    const cookiestring = RegExp("" + this.sCookieName + "[^;]+").exec(document.cookie);
    // Return everything after the equal sign, or an empty string if the cookie name not found
    return decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");
  };
  
  /**
  * Delete our own cookie
  * NOTE: Actually set the cookie time to unix 0 so it will get deleted automatically
  *
  * @return null
  */
  AltavistaGetResponse.prototype.deleteCookie = function() {
    let d = new Date();
    // document.cookie = this.sCookieName + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    d.setTime(d.getTime() - (1 * 24 * 60 * 60 * 1000)); // expires yesterday
    const expires = "expires="+ d.toUTCString();
    document.cookie = this.sCookieName + "=" + ";" + expires + ";path=/";
  };
  
  /**
  * Method to be added inside the page that contains the trigger to the `visited page`
  * event for GetResponse. The page should have a form of some kind to which we can
  * hook to with its `submit` event.
  *
  * @param form the unique identifier (html ID) of the form.
  */
  AltavistaGetResponse.prototype.registerForm = function(form, emailFieldId) {
    if (!form) {
      throw new Error("[AltavistGetResponse.registerForm] Missing identifier for form.");
    }
    $form = document.getElementById(form);
    
    if (!$form)
      throw new Error('[AltavistGetResponse.registerForm] could not find requested form:', form);
    
    var $ = $ || jQuery;
    var self = this;
    $form.addEventListener('submit', function(evt) {
      const emailField = emailFieldId;
      // TODO: for full compatibility remove jquery and use vanilla JS to get value
      const email = $('#'+emailField).val()
      self.setCookie(email);
    });
    
    // form should be either a string for the form ID or the number to find it into document.forms
    // if the form is not found we should throw an error of sort so we can know that something happened
  };
  
  /**
  * Extract the previously set cookie (if any) and dispatch the value (user email)
  * to GetRespnse, then deletes the cookie.
  *
  * @return null
  */
  AltavistaGetResponse.prototype.notify = function() {
    const email = this.getCookie();
    // fail silently, since we actually don't care. What happens is that the page
    // was visited without passing through the page that sets the cookie.
    // if (!email) return console.warn("[AltavistaGetResponse.notify] No cookie value found");
    window.gaSetUserId(email);
    this.deleteCookie(this.sCookieName);
  };
  
  // Execution ---------------------------------------------------------------
  
  window.avgr = new AltavistaGetResponse(name, time);
  
}("userEmail", 31));
